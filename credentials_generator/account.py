import sys
import subprocess as sp
from subprocess import Popen, PIPE
import json
import ast
from urllib.parse import unquote

username = str(sys.argv[1])
password = str(sys.argv[2])
plan = str(sys.argv[3])

class Service():
    def __init__(self, name, plan):
        self.name = name
        self.credentials = {}
        self.plan = plan
        self.role = "Editor"
        self.instance_name = "TJBot_" + name + plan
        self.key_name = self.instance_name + "_Key"
        
        # Customizations for exceptions
        if self.name == "language-translator":
            self.role = ""

        if self.name == "watson-vision-combined":
            self.plan = "lite"

        # Creating resource if not found
        if not self.check_res():
            print("Creating " + self.name + " service")
            sp.call(["ibmcloud", "resource", "service-instance-create", self.instance_name, self.name, self.plan, "us-south"])
        
        # Creating key if not found
        if not self.check_key():
            print("Creating " + self.name + " key")
            p = Popen(["ibmcloud", "resource", "service-key-create", self.key_name, "Editor", "--instance-name", self.instance_name], stdout=PIPE, stderr=PIPE)
            out, err = p.communicate()
            p.wait()
            print(out.decode("utf-8"))

    def check_res(self):
        # Command to check the existance of a certain resource
        print("Checking if " + self.name + " resource exist")
        p = Popen(["ibmcloud", "resource", "service-instance", "--output", "JSON", self.instance_name], stdout=PIPE, stderr=PIPE)
        out, err = p.communicate()
        p.wait()

        found = False

        # Decoding output
        output = out.decode("utf-8")
        print(output)
        output = output.replace("\n", "").replace(" ", "").replace("null", "\"\"")

        res = []
        try:
            res = ast.literal_eval(output)
            print(self.name + " res list:")
            print(res)
        except:
            print("Error in evaluating output res")
            return False

        # Analyzing keys to find the needed one
        for k in res:
            print("Analizing res:")
            print(k)
            if k["name"] == self.instance_name:
                found = True
                print("Found " + self.name + " res: " + self.instance_name)
                break
        return found

    def check_key(self):
        # Command to check the existance of a certain key
        print("Checking if " + self.name + " key exist")
        p = Popen(["ibmcloud", "resource", "service-key", "--output", "JSON", self.key_name], stdout=PIPE, stderr=PIPE)
        out, err = p.communicate()
        p.wait()

        found = False

        # Decoding output
        output = out.decode("utf-8")
        print(output)
        output = output.replace("\n", "").replace(" ", "").replace("null", "\"\"")        

        keys = []
        try:
            keys = ast.literal_eval(output)
            print(self.name + " keys list:")
            print(keys)
        except:
            print("Error in evaluating output keys")
            return False

        # Analyzing keys to find the needed one
        for k in keys:
            print("Analizing key:")
            print(k)
            if k["name"] == self.key_name:
                found = True
                self.credentials["url"] = k["url"]
                self.credentials["apikey"] = k["credentials"]["apikey"]
                print("Found " + self.name + " key: " + self.credentials["apikey"])
                break
        return found


print("Disabling updates")
sp.call(["ibmcloud", "config", "--check-version=false"])

print("Loggin in")
sp.call(["ibmcloud", "login", "-u", username, "-p", password, "-a", "https://api.eu-gb.bluemix.net"])

print("Setting target")
sp.call(["ibmcloud", "target", "-o", username, "-s", "dev"])

# DOING ALL THE STUFF
print("Generating TJBot services")
service_names = ["text-to-speech", "speech-to-text", "tone-analyzer", "language-translator", "watson-assistant", "watson-vision-combined"]
services = {}
config = {}

for s in service_names:
    # Create services objects
    services[s] = Service(s, plan)    

    # Populate config dictionary
    config[s] = services[s].credentials

for k in config:
    # Replacing different strings in keys
    k.replace("watson-vision-combined", "visual recognition")
    k.replace("-", " ")

    # Decode url field
    try:
        config[k]["url"] = unquote(config[k]["url"])
    except Exception as e:
        print("Error in decoding url field for credentials")
        print(e)        


# RESPONSE OUTPUT
# with open("tjbot_config.json", "w") as outfile:
#     outfile.write(json.dumps(config, indent=2))
print("TJBOTASPHI_RESPONSE" + str(config) + "TJBOTASPHI_ENDRESPONSE")

print("Done")
