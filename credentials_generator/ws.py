from flask import Flask
from flask import request
import os
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/cred", methods=['GET'])
def cred():
    username = request.args.get('username')
    password = request.args.get('password')
    os.system("python3 account.py " + username + " " + password)
    with open("tjbot_config.json", "r") as myfile:
        data=myfile.read()
    return data

app.run()