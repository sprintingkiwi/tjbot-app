#!/bin/bash
USERNAME=$1
PASSWORD=$2

echo "Loggin in"
ibmcloud login -u $USERNAME -p $PASSWORD

echo "Setting target"
bx target -o $USERNAME -s dev

# SERVICES=("text_to_speech" "speech_to_text" "tone_analyzer" "language_translator" "conversation")
# for N in ${#SERVICES[@]}
# do
#     echo "creating ${SERVICES[$N]} ..."
#     bx service create ${SERVICES[$N]} standard TJBot_${SERVICES[$N]}
#     echo "done"
# done

echo "Creating services"
bx service create text_to_speech standard TJBot_tts
bx service create speech_to_text standard TJBot_stt
bx service create tone_analyzer standard TJBot_ta
bx service create language_translator standard TJBot_lt
bx service create conversation standard TJBot_conv

echo "Creating credentials"
bx cf create-service-key TJBot_tts TJBot_tts_key
bx cf create-service-key TJBot_stt TJBot_stt_key
bx cf create-service-key TJBot_ta TJBot_ta_key
bx cf create-service-key TJBot_lt TJBot_lt_key
bx cf create-service-key TJBot_conv TJBot_conv_key