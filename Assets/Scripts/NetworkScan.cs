﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.Networking;

public class NetworkScan : MonoBehaviour {
    public List<string> TJBotIps = new List<string>();
    public List<Coroutine> ScanCoroutines = new List<Coroutine>();
    public bool scan = false;    
    public List<TJBot> TJBots = new List<TJBot>();
    Image mr;

    private static bool created = false;

    void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(this.gameObject);
            created = true;
            Debug.Log("Awake: " + this.gameObject);
        }
    }

    void Start()
    {
        
    }

    public void Scan()
    {
        mr = GameObject.Find("Start Network Scan").GetComponent<Image>();
        mr.color = Color.white;

        if (scan)
        {
            Debug.Log("Stopping Scanner");
            if (ScanCoroutines.Count > 0)
                foreach (Coroutine c in ScanCoroutines.ToArray())
                {
                    if (c != null)
                    {
                        ScanCoroutines.Remove(c);
                        StopCoroutine(c);
                    }                    
                }
            Debug.Log("Active Coroutines: " + ScanCoroutines.Count);

            scan = false;
        }
        else
        {
            scan = true;

            string ip = Network.player.ipAddress;
            Debug.Log("My ip addess is " + ip);
            string[] parts = ip.Split('.');
            parts = parts.Take(parts.Count() - 1).ToArray();
            string ipRoot = "";
            foreach (string s in parts)
                ipRoot += s + ".";
            List<string> ips = new List<string>();

            Debug.Log("Scannig this addesses: ");
            for (int i = 0; i < 255; i++)
            {
                ips.Add(ipRoot + i);
                Debug.Log(ips[i]);
                Debug.Log("Sending web request");
                ScanCoroutines.Add(StartCoroutine(SearchTJBot(ips[i])));
            }
            StartCoroutine(ScanFeedback());
        }     
    }

    public IEnumerator SearchTJBot(string ip)
    {
        if (TJBotIps.Contains(ip))
            yield break;

        string url = "http://" + ip + ":3000/test";

        using (UnityWebRequest request = UnityWebRequest.Get(url))
        {
            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.Log(ip + ": " + request.error);
            }
            else
            {
                // Show results as text
                if (request.downloadHandler.text == "tjbot")
                {
                    Debug.Log("Found TJBot at ip address " + ip);
                    TJBotIps.Add(ip);
                    yield return StartCoroutine(CreateTJBotObject(ip));
                }
                else if (TJBotIps.Contains(ip))
                {
                    Debug.Log(ip +  " is no more connected. Destroying it...");
                    Destroy(GameObject.Find(ip));
                    TJBotIps.Remove(ip);
                }

                // Or retrieve results as binary data
                //byte[] results = request.downloadHandler.data;
            }
        }
    }

    public IEnumerator CreateTJBotObject(string TJBotIp)
    {
        Debug.Log("Creating TJBot " + TJBotIp);

        Transform TJContainer = GameObject.Find("TJBOTS").transform;
        TJBot tj = Instantiate(Resources.Load("TJBot") as GameObject, TJContainer).GetComponent<TJBot>();
        tj.name = TJBotIp;
        TJBots.Add(tj);
        tj.ID = TJBots.Count;
        tj.Setup();

        yield return null;
    }

    public IEnumerator ScanFeedback()
    {
        while (ScanCoroutines.Count > 0)
        {
            mr.color = Color.white;
            yield return new WaitForSeconds(0.5f);
            mr.color = Color.blue;
            yield return new WaitForSeconds(0.5f);
        }
        mr.color = Color.white;
        yield return null;
    }
}
