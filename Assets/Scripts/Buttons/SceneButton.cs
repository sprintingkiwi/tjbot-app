﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneButton : ButtonUtils
{
    public string scene;

    public void OnMouseDown()
    {
        SceneManager.LoadScene(scene);
    }
}
