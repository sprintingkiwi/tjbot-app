﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ButtonUtils : MonoBehaviour {

    public Controller controller;
    
    //[Header("Function")]
    //public UnityEvent myUnityEvent;

    // Use this for initialization
    void Start ()
    {
        controller = GameObject.Find("Controller").GetComponent<Controller>();
    }

    // Update is called once per frame
    void Update ()
    {
		
	}

    //public virtual void OnMouseDown()
    //{
    //    Debug.Log(name + " clicked");
    //    myUnityEvent.Invoke();
    //}

    public void SnapLink()
    {
        if (controller.activeTJBot == null)
        {
            Debug.Log("You need to select one TJBot to start programming with SNAP!");
            return;
        }

        Debug.Log("Running SNAP");
        System.Diagnostics.Process.Start("http://" + controller.activeTJBot.ip + ":3000/snap");
    }

    public void StartNetworkScan()
    {
        controller.ns.Scan();
    }
}
