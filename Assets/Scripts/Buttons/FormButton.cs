﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FormButton : ButtonUtils
{
    public InputField[] inputFields;
    public Dropdown[] dropdown;

    public bool MissingInput()
    {
        foreach (InputField i in inputFields)
        {
            if (i == null)
            {
                Debug.LogWarning("Missing input " + i.name);
                return true;
            }
        }

        return false;
    }

    public void StartConversation()
    {
        if (MissingInput())
            return;
        StartCoroutine(controller.tjConversation.Converse(inputFields[0].text));
    }

    public void StartAccountCreation()
    {
        if (MissingInput())
            return;
        Debug.Log("Starting account creation");
        StartCoroutine(controller.GenerateCredentials(inputFields[0].text, inputFields[1].text, dropdown[0].value, transform));
    }

    public void SendCredentials()
    {
        if (controller.activeTJBot == null)
        {
            Debug.LogWarning("No active TJBot");
            return;
        }

        if (controller.credentials == "")
        {
            Debug.LogWarning("No credentials set");
        }

        StartCoroutine(controller.activeTJBot.TJCall("config/" + controller.credentials, Debug.Log));
    }

}
