﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TJBot : MonoBehaviour
{
    public string ip;
    public int ID;
    Controller controller;
    MeshRenderer mr;

    public void Setup()
    {
        Debug.Log("Setupping TJBot " + name);
        controller = GameObject.Find("Controller").GetComponent<Controller>();
        mr = transform.Find("head").GetComponent<MeshRenderer>();
        mr.material.color = Color.grey;
        ip = name;
        FindAPlace();
        transform.Find("name").GetComponent<TextMesh>().text = name;
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void OnMouseDown()
    {
        Debug.Log("Clicked " + name);    

        if (controller.activeTJBot == this)
        {
            controller.activeTJBot = null;
            mr.material.color = Color.grey;
            return;
        }

        if (controller.activeTJBot != null)
        {
            controller.activeTJBot.mr.material.color = Color.grey;
        }

        controller.activeTJBot = this;
        mr.material.color = Color.yellow;
        StartCoroutine(Blink());


    }

    public IEnumerator TJCall(string method, System.Action<string> callback = null)
    {
        string url = "http://" + ip + ":3000/" + method;

        Debug.Log("Called " + url);

        using (UnityWebRequest request = UnityWebRequest.Get(url))
        {
            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.Log(request.error);
            }
            else
            {
                // Show results as text
                if (callback != null)
                {
                    string response = request.downloadHandler.text;
                    Debug.Log("Calling callback of " + url);
                    callback(response);
                }

                // Or retrieve results as binary data
                //byte[] results = request.downloadHandler.data;
            }
        }
    }

    // Ping Behaviour
    public IEnumerator Blink()
    {
        StartCoroutine(TJCall("play/ping.wav"));
        yield return StartCoroutine(TJCall("led/255/0/0"));
        yield return new WaitForSeconds(0.2f);
        yield return StartCoroutine(TJCall("led/0/255/0"));
        yield return new WaitForSeconds(0.2f);
        yield return StartCoroutine(TJCall("led/0/0/255"));
        yield return new WaitForSeconds(0.2f);
        yield return StartCoroutine(TJCall("led/0/0/0"));
    }

    public void FindAPlace()
    {
        transform.position = new Vector3(-9f + transform.localScale.x * 1.3f * ID, -2f, 0f);
    }
}
