﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.Networking;
using System;

public class Controller : MonoBehaviour
{
    public TJBot_Conversation tjConversation;
    public string credentials;
    public NetworkScan ns;
    public TJBot activeTJBot;

    private static bool created = false;

    void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(this.gameObject);
            created = true;
            Debug.Log("Awake: " + this.gameObject);
        }
    }

    // Use this for initialization
    void Start ()
    {
        ns = GameObject.Find("Network Scan").GetComponent<NetworkScan>();
        tjConversation = GameObject.Find("Conversation").GetComponent<TJBot_Conversation>();
        Debug.Log("Initializing " + tjConversation.name);
        tjConversation.Setup();
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public IEnumerator GenerateCredentials(string username, string password, int plan, Transform button)
    {
        string planName;
        if (plan == 0)
            planName = "lite";
        else
            planName = "standard";


        Text resText = GameObject.Find("ACCOUNT").transform.Find("Response").GetComponent<Text>();

        if (username == "")
        {
            resText.text = "Missing USERNAME and/or PASSWORD";
            yield break;
        }
        Debug.Log("Sending request to generate credentials for " + username);
        //button.GetComponent<MeshRenderer>().material.color = Color.blue;
        //Rigidbody rb = button.GetComponent<Rigidbody>();
        //rb.AddTorque(Vector3.right * 20f);
        yield return null;

        //string url = "http://192.168.1.144:5000/cred?username=" + username + "&password=" + password;
        string url = "http://5.144.189.58/script/launcher.sh?" + username + "&" + password + "&" + planName;

        using (UnityWebRequest request = UnityWebRequest.Get(url))
        {
            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.Log(request.error);
                resText.text = request.error;
                //button.GetComponent<MeshRenderer>().material.color = Color.red;
                //rb.angularVelocity = Vector3.zero;
                //button.rotation = Quaternion.Euler(Vector3.zero);
            }
            else
            {
                // Show results as text
                string response = request.downloadHandler.text;
                credentials = response.Split(new string[] { "TJBOTASPHI_RESPONSE" }, StringSplitOptions.None)[1];
                credentials = credentials.Split(new string[] { "TJBOTASPHI_ENDRESPONSE" }, StringSplitOptions.None)[0];
                credentials = credentials.Replace("\n", "").Replace("/", "%2F").Replace("\'", "\"");
                Debug.Log(response);
                //resText.text = credentials;
                //rb.angularVelocity = Vector3.zero;
                //button.rotation = Quaternion.Euler(Vector3.zero);
                //button.GetComponent<MeshRenderer>().material.color = Color.green;

                // Or retrieve results as binary data
                //byte[] results = request.downloadHandler.data;
            }

            yield return null;
        }

        yield return null;
    }
}
